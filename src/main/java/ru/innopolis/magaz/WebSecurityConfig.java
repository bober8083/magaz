package ru.innopolis.magaz;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private UserRepo userRepo;

    public WebSecurityConfig(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(authorize -> authorize
                        .antMatchers("/").permitAll()
                        .antMatchers("/index").permitAll()
                        .anyRequest().authenticated()
                )
                .formLogin(withDefaults());
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        List<UserDetails> userDetails = new ArrayList<>();

        List<MagazUser> users = userRepo.findAll();
        for (MagazUser curr : users) {
            userDetails.add(User.withDefaultPasswordEncoder()
                    .username(curr.getUser())
                    .password(curr.getPassword())
                    .roles(curr.getRole())
                    .build());
        }

        UserDetails user =
                User.withDefaultPasswordEncoder()
                        .username("denys")
                        .password("denyspassword")
                        .roles("USER")
                        .build();

        userDetails.add(user);
        return new InMemoryUserDetailsManager(userDetails);
    }
}
