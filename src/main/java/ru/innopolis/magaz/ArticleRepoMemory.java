package ru.innopolis.magaz;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class ArticleRepoMemory {
    List<Article> articles = new ArrayList<>();

    public ArticleRepoMemory() {
        articles.add(new Article(1L, "test 1", BigDecimal.valueOf(1.121)));
        articles.add(new Article(2L, "test 2", BigDecimal.valueOf(5.121)));
    }

    public List<Article> findAll() {
        return articles;
    }

    public void addArticle(Article newArticle) {
        articles.add(newArticle);
    }
}
