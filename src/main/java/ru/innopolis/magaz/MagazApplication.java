package ru.innopolis.magaz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MagazApplication {
	public static void main(String[] args) {
		SpringApplication.run(MagazApplication.class, args);
	}

}
