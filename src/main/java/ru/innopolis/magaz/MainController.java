package ru.innopolis.magaz;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.math.BigDecimal;
import java.util.List;

@Controller
public class MainController {
    private ArticleRepoMemory repo;
    private UserRepo userRepo;

    public MainController(ArticleRepoMemory repo,
                          UserRepo userRepo) {
        this.repo = repo;
        this.userRepo = userRepo;
    }

    @GetMapping(value = "/")
    public String root() {
        return "redirect:index";
    }

    @GetMapping(value = "/index")
    public String index() {
        return "index";
    }

    @GetMapping(value = "/list")
    public String list(Model model) {
        List<Article> articles = repo.findAll();
        model.addAttribute("articles", articles);
        return "list";
    }

    @GetMapping(value = "/addArticle")
    public String addArticle(Model model) {
        CreateArticleDto createArticleDto = new CreateArticleDto();
        createArticleDto.title = "<Input here>";
        model.addAttribute("createArticleForm", createArticleDto);
        return "addArticle";
    }

    @PostMapping(value = "/createArticle")
    public String createArticle(@ModelAttribute("title") CreateArticleDto dto,
                                Model model) {
        repo.addArticle(fromDto(dto));;
        return "redirect:list";
    }

    private static Article fromDto(CreateArticleDto dto) {
        BigDecimal price = new BigDecimal(dto.getPrice());
        Article newArticle = new Article(dto.getTitle(), price);
        return newArticle;
    }
}
